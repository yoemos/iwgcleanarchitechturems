﻿using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Application.Common.Models;
using IWG.Heimdall.Domain.Common;
using MassTransit.Mediator;
using Microsoft.Extensions.Logging;

namespace IWG.Heimdall.Infrastructure.Services;

public class DomainEventService : IDomainEventService
{
    private readonly ILogger<DomainEventService> _logger;
    private readonly IMediator _mediator;

    public DomainEventService(ILogger<DomainEventService> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    public async Task Publish(DomainEvent domainEvent)
    {
        _logger.LogInformation("Publishing domain event. Event - {event}", domainEvent.GetType().Name);
        await _mediator.Publish(GetNotificationCorrespondingToDomainEvent(domainEvent));
    }

    private object GetNotificationCorrespondingToDomainEvent(DomainEvent domainEvent)
    {
        return Activator.CreateInstance(typeof(DomainEventNotification<>).MakeGenericType(domainEvent.GetType()), domainEvent)!;
    }
}
