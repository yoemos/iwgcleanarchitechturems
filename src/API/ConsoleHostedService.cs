﻿using MassTransit;
using Microsoft.Extensions.Hosting;

namespace IWG.Heimdall.API;

internal class ConsoleHostedService : IHostedService
{
    private readonly IBusControl _busControl;
    public ConsoleHostedService(IBusControl bus)
    {
        _busControl = bus;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {        
        await _busControl.StartAsync(cancellationToken);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await  _busControl.StopAsync(cancellationToken);
    }
}
