﻿using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using IWG.Heimdall.API;
using IWG.Heimdall.Application;
using MassTransit;
using MassTransit.Mediator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IWG.Heimdall.Program;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var host = CreateHostBuilder(args)
            .UseConsoleLifetime()
            .Build();
        await host.RunAsync();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return
            Host.CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
            .ConfigureContainer<ContainerBuilder>(ConfigureAutofac)
            .ConfigureServices(ConfigureServcices)
            .ConfigureHostConfiguration(config =>
            {
                // Note: it should be DOTNET_ for generic host and ASPNETCORE_ by default for web host, but it is just a prefix, and just to follow same naming used in other services
                // Note: this can be changed in environment variables then this step can be removed, also should be reflected in launchSettings.json
                config.AddEnvironmentVariables(prefix: "ASPNETCORE_");
            })
            .ConfigureAppConfiguration((hostingContext, configBuilder) =>
            {
                var basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var env = hostingContext.HostingEnvironment;
                configBuilder.SetBasePath(basePath)
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

                configBuilder.AddEnvironmentVariables();

                if (args != null)
                {
                    configBuilder.AddCommandLine(args);
                }
            })
            ;//.ConfigureLogging(ConfigureLogging);
    }

    private static void ConfigureServcices(HostBuilderContext hostContext, IServiceCollection services)
    {
        services.AddOptions();        
       
        services.AddMassTransitServices(hostContext.Configuration);
        
        services.AddHostedService<ConsoleHostedService>();

    }
    private static void ConfigureAutofac(HostBuilderContext hostBuilderContext, ContainerBuilder containerBuilder)
    {
        containerBuilder.RegisterModule(new AutofacModule());
    }


    static IServiceCollection AddMassTransitServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddMassTransit(x =>
        {
            x.UsingAzureServiceBus((context, cfg) =>
            {
                cfg.Host(configuration.GetValue<string>("MassTransit:ConnectionString"));
                MassTransitEndpoints.Register(cfg);
            });
        });

        IMediator mediator = Bus.Factory.CreateMediator(cfg =>
        {
        });

        services.AddSingleton(mediator);
        
        MassTransitConsumers.Register(mediator);

        return services;
    }

}