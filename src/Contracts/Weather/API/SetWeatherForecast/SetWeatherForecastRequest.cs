﻿namespace IWG.Heimdall.Contracts.Weather.API.SetWeatherForecast;
public record SetWeatherForecastRequest(DateTime Date, double Temperature);
