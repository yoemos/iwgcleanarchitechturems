﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWG.Heimdall.Contracts;

public record WeatherForecast( DateTime Day, double Temperature);

