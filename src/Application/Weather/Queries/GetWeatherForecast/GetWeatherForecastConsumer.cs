﻿using MassTransit;

namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

internal class GetWeatherForecastConsumer : IConsumer<GetWeatherForecastRequest>
{

    static readonly List<WeatherForecast> Temperatures = new List<WeatherForecast>
            {
                new WeatherForecast(new DateTime(2020, 10, 10), 12),
                new WeatherForecast(new DateTime(2020, 10, 11), 13),
                new WeatherForecast(new DateTime(2020, 10, 12), 11),
            };
    public async Task Consume(ConsumeContext<GetWeatherForecastRequest> context)
    {
        var temperatureDayRange = Temperatures.Where(x => x.Day >= context.Message.FromDate && x.Day <= context.Message.ToDate).ToList();
        await context.RespondAsync(new GetWeatherForecastResponse(temperatureDayRange));
    }
}
