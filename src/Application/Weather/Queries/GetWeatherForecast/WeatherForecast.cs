﻿namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

public record WeatherForecast(DateTime Day, double Temperature);