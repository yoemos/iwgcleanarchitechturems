﻿namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

public record GetWeatherForecastRequest(DateTime FromDate, DateTime ToDate);

