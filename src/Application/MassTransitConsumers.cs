﻿using IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;
using MassTransit;
using MassTransit.Mediator;

namespace IWG.Heimdall.Application;

public class MassTransitConsumers
{
    public static void Register(IMediator mediator)
    {
        mediator.ConnectConsumer<GetWeatherForecastConsumer>();
    }
}
