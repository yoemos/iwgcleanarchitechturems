﻿namespace IWG.Heimdall.Application.Common.Interfaces;

public interface ICurrentUserService
{
    string? UserId { get; }
}
