﻿using IWG.Heimdall.Domain.Common;
using MassTransit.Mediator;
namespace IWG.Heimdall.Application.Common.Models;

public class DomainEventNotification<TDomainEvent> where TDomainEvent : DomainEvent
{
    public DomainEventNotification(TDomainEvent domainEvent)
    {
        DomainEvent = domainEvent;
    }

    public TDomainEvent DomainEvent { get; }
}
